#!/usr/bin/env python
import sys
import os
import hashlib
#####################################

from django.core.mail import send_mail
from  smsandemailstring import *
 ############################################
sys.path.insert(0, os.path.realpath(os.getcwd()))
from django.conf import settings
from  settings import LOCALMACHINE,EMAIL_HOST_USER
from tornado.options import options, define, parse_command_line
import django.core.handlers.wsgi
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi
import jsonpickle # used to encode the complex python object into json string
from utils import *
import random
from urllib2 import urlopen
from urllib import urlencode
from models import Organization , User, MeetingRoom, Meeting,Participant,Usage
import xml.etree.ElementTree as ET
from LogMgr.logger import *
from webresponse import WebReturn,jsonresponse
from tornado.escape import json_decode, json_encode
from django.core.serializers.json import DjangoJSONEncoder
from datetime import timedelta
import datetime
define('port', type=int, default=8086)

import httplib
from meetinghandler.meetinghandler import BBBMeeting, GetBBBWelcomeMsg
from LogMgr.logger import YCSLogger 
from decorator.auth import * 
logger = YCSLogger()

LOG_DEBUG = YCSLogger.logger.debug
LOG_WARNING = YCSLogger.logger.warning
LOG_FATAL = YCSLogger.logger.fatal
LOG_ERROR = YCSLogger.logger.error
LOG_INFO = YCSLogger.logger.info
from django.core import serializers
import time
from datetime import datetime
GLOBAL_URL_LOGOUT = 'http://f2fapi.talismanuc.com:8086/logout/'


def parse(response):
    try:
        xml = ET.XML(response)
        code = xml.find('returncode').text
        if code == 'SUCCESS':
            return xml
        else:
            raise
    except:
        return None

class OrgHandler(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self,receive_org_name):
        if not APIAuth.checkHeader(self.request.headers) :
            return WebReturn(self, httplib.FORBIDDEN, {'reason':'invalid API access'})
        data_to_verify = ['notes','secondary_email','secondary_landline','secondary_mobile','status','org_type','abn_acn_no', 'org_url', 'city', 'state', 'country','street', 'pin', 'primary_mobile', 'primary_landline', 'primary_email']
        errors = {}
        try:receive_org_name 
        except:
			LOG_WARNING('Invalid Url parameter  %s ',receive_org_name )
			return WebReturn(self,httplib.FORBIDDEN, {'reason':"Please pass a organization name along url "},{'data':receive_org_name})
        try:
            body = self.request.body
            body_loads = loads(body)
        except Exception as e:
            LOG_WARNING('Exception: %s', str(e))
            return WebReturn(self,httplib.FORBIDDEN, {'reason':'Body is not Define '},{'data':receive_org_name})
            get = None   
        get = dict2obj(body_loads)
        get.org_url
        try:already = Organization.objects.get(org_name=receive_org_name)
        except:already = False
        if already:
            return WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'This organization is already registered'},{'data':receive_org_name})
        org_new_object = Organization()
        for k,v in body_loads.items():
            if k in data_to_verify:
                try:
                    setattr(org_new_object, k, v)
                except:
                    pass
        x = org_new_object.Validate()
        if x == {}:
            y = None
            LOG_INFO('Date received : %s ', body_loads.items())
            org_new_object.org_name = receive_org_name
            org_new_object.save()
            return WebReturn(self,httplib.OK, y,{'Message ':'Data Added successfully'})   
        else:
            LOG_INFO('Date Format: %s ', body_loads.items())
            return WebReturn(self,httplib.BAD_REQUEST,x,{'organization':receive_org_name})   
    @ValidatedAPI  
    def get(self, orgname):
        try:
            check_exist = Organization.objects.filter(org_name=orgname).exclude(status = 0)
        except:
            check_exist = False
        if not  check_exist:
            LOG_INFO('Organization %s is not present in our database', orgname )
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'organization does not exist'},{'organization':orgname})   
        else :
            errors = None
 #           data =  json.dumps(check_exist, cls=DjangoJSONEncoder)
            print "Json  is not serializable " 
 		    
            #get_dump = dumps(check_exist)
            return  WebReturn(self,httplib.OK,errors,check_exist)
    @ValidatedAPI          
    def delete(self, org_name):
        try:
            Chk_org = Organization.objects.get(org_name=org_name)
        except:
            Chk_org = False
            LOG_INFO('Organization %s is not present in our database', org_name )

            WebReturn(self,httplib.NOT_FOUND,{'reason':'organization does not exist'},{'organization':org_name})   
        
        if not Chk_org.status == 0:
            Chk_org.status = 0
            Chk_org.disable_other()
  
            Chk_org.save()
            LOG_INFO('Organization %s is not present in our database', org_name )
            y = None
            return WebReturn(self,httplib.OK,y,{'Success':'Organization and associated  users has been deleted successfully'})   
        else:
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':'Organization does not longer exist'},org_name)   
            
    @ValidatedAPI       
    def put(self, org_name):
        
        data_to_verify = ['notes','secondary_email','secondary_landline','secondary_mobile','status','org_type','abn_acn_no', 'org_url', 'city', 'state', 'country','street', 'pin', 'primary_mobile', 'primary_landline', 'primary_email']
        
        try:
            org_to_be_update = Organization.objects.get(org_name=org_name)
        except:
            org_to_be_update = False
            LOG_INFO('Organization %s is not present in our database', org_name )
            return WebReturn(self,httplib.FORBIDDEN, {'reason':"organization  does not not exist"},{'data':org_name})
        if org_to_be_update:
            try:
                body = self.request.body
                body_loads = loads(body)
            except:
                LOG_WARNING('Please enter the correct data format for  %s ', org_name )
                return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':"Body not define "},{'data':org_name})
                get = None   
            get = dict2obj(body_loads)
            for k,v in body_loads.items():
                if k in data_to_verify:
                    try:
                        setattr(org_to_be_update, k, v)
                    except:
                        pass
            x = org_to_be_update.Validate()
            if x == {}:
                    org_to_be_update.save()
                    y = 'None' 
                    LOG_INFO('Organization %s updated successfully ', org_name )
                    return WebReturn(self,httplib.OK,y,{'Message ':'Data Update successfully'})   
            else:
                    LOG_WARNING('Validation error ', org_name)
                    return WebReturn(self,httplib.BAD_REQUEST,x,{'organization':org_name})   
       
class UserHandler(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self,user_name):
        data_to_verify = ['timezone','user_type','middle_name','first_name','city','state','country','street','pin','last_name','first_name', 'passwd', 'address', 'date_created', 'primary_mobile', 'primary_landline', 'primary_email','secondary_mobile', 'secondary_landline','secondary_email']
        try:user_name 
        except:
            LOG_WARNING('Invalid Url parameter ', + str(user_name))
            return WebReturn(self,httplib.FORBIDDEN, {'reason':"Please pass a user name along url "},{'data':user_name})
        if user_name == '':
            return WebReturn(self,httplib.FORBIDDEN,{'reason':'Please pass a valid user name along url'},{'data':user_name})
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s',user_name )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':user_name})
            get = None 
             
        get = dict2obj(body_loads)
        try:get_org = Organization.objects.get(org_name = get.org_name)
        except:
            
            LOG_WARNING('This organization doest not exist in our database ')
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Please enter a valid organization name "},{'data':user_name})
        if get_org.status == 0:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"This organization doest not longer exist  "},{'data':get.org_name})
        usr = User()
        for k,v in body_loads.items():
            if k in data_to_verify:
                try:
                    setattr(usr, k, v)
                except:
                    pass
        get_validate_response = usr.Validate() 
        if get_validate_response == {}:
            usr.passwd = hashlib.sha1(usr.passwd).hexdigest()
            try:get_already = User.objects.get(user_name =user_name)
            except:get_already = False
            if not get_already:
                usr.user_name = user_name
               
                usr.org_name = get_org
                usr.save()
                LOG_INFO('User named %s  has been added ',usr.user_name)
            else:
                return WebReturn(self,httplib.BAD_REQUEST,{'reason':'User already exist '},{'user_name':'User already registered '})
            y = None 
            MeetingRecord(usr,'NULL','NULL','User created successfully ','user name' + usr.user_name)
            return WebReturn(self,httplib.OK,y,{'Message ':'Data Added successfully'})   
        else:
            LOG_WARNING('Validation error  with  %s  ',get_validate_response )
            return WebReturn(self,httplib.BAD_REQUEST,get_validate_response,{'User':user_name})                   
    @ValidatedAPI  
    def get(self, user_name):
        try:
            check_exist = User.objects.defer("passwd").get(user_name=user_name)
        except:
            check_exist = False
        if not  check_exist:
            LOG_WARNING('Username %s doest not exist   ',user_name )
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'User does not exist'},{'Username':user_name})   
        if check_exist.status == 0:
            return  WebReturn(self,httplib.UNAUTHORIZED,{'reason':'This user does not longer  exist'},{'Username':user_name})   
        else :
            y = None
            get_dump = jsonpickle.encode(check_exist)
            LOG_INFO('User details  %s ',get_dump )            
            return  WebReturn(self,httplib.OK,y,get_dump)   
    @ValidatedAPI  
    def delete(self, user_name):
        try:
            Chk_usr =  User.objects.get(user_name=user_name)   
        except:
            Chk_usr = False
            LOG_WARNING('Username %s doest not exist   ',user_name )
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':'user_name does not exist'},{'Username':user_name})   
        if Chk_usr.status == 0:
            return  WebReturn(self,httplib.UNAUTHORIZED,{'reason':'This user does not longer exist'},{'Username':user_name}) 
        if Chk_usr:
            Chk_usr.status = 0
            Chk_usr.disable_from_user()
           
            LOG_INFO('User %s  has been deleted successfully  %s ',user_name )            
            MeetingRecord(Chk_usr,'NULL','NULL','User deleted  successfully ','user name \t' + Chk_usr.user_name)
            Chk_usr.save()
            return WebReturn(self,httplib.OK,{'errors':'None'},{'Success':' user has been deleted successfully'})   
        
    @ValidatedAPI  
    def put(self, user_name):
        data_to_verify = ['timezone','user_type','middle_name','first_name','city','state','country','street','pin','last_name','first_name','address', 'date_created', 'primary_mobile', 'primary_landline', 'primary_email','secondary_mobile', 'secondary_landline','secondary_email']

        try:user_name 
        except:
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Please pass a user_name name along url "},{'data':user_name})
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s ',user_name )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':user_name})
            get = None  
        get = dict2obj(body_loads)
        
        try:get_first_name = get.first_name
        except:get_first_name = False
        try:
            usr = User.objects.get(user_name=user_name)
        except:
            LOG_INFO('User %s is not present in our database', user_name )
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Username does not exist "},{'data':user_name})
        if usr.status == 0:
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'This user does not longer  exist'},{'Username':user_name})         
        for k,v in body_loads.items():
            if k in data_to_verify:
                try:
                    setattr(usr, k, v)
                except:
                    pass
        get_validate_response = usr.Validate()  
        if get_validate_response == {}:
            if get_first_name : usr.first_name = get_first_name
            usr.save()
            y = None 
            LOG_INFO('User  %s   Updated ',usr.user_name)
            MeetingRecord(usr,'NULL','NULL','User update   successfully ','user name \t' + usr.user_name)
            return WebReturn(self,httplib.OK,y,{'Message ':'Data Updated successfully'})   
        else:
            LOG_WARNING('Validation error  with  %s  ',get_validate_response )
            
            return WebReturn(self,httplib.BAD_REQUEST,get_validate_response,{'User':user_name})        

class UserChangePassword(tornado.web.RequestHandler):
    @ValidatedAPI  
    def put(self, user_name):
      
        data_to_verify = ['password', 'new_password']
        try:
            user_to_be_update = User.objects.get(user_name=user_name)
        except:
            json_mess = ()    
            user_to_be_update = False
            LOG_INFO('User %s is not present in our database', user_name )
            return WebReturn(self,httplib.FORBIDDEN,{'user':'User does  not exist'},{'User':user_name})        
        if user_to_be_update.status == 0:
            return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'This user does not longer  exist'},{'Username':user_name}) 
        if user_to_be_update:
            try:
                body = self.request.body
                body_loads = loads(body)
                get = dict2obj(body_loads)
                print body_loads
            except:
                get = None
                LOG_WARNING('Body  is not in correct  format  %s ',user_name )
                return WebReturn(self,httplib.NOT_ACCEPTABLE,{'user':'Body not define '},{'User':user_name})        
            if get:
                get_list_obj_to_update = verify_data(get, data_to_verify)
                if not get_list_obj_to_update :
                    if user_to_be_update.passwd == hashlib.sha1(get.password).hexdigest():
                        user_to_be_update.passwd = hashlib.sha1(get.new_password).hexdigest()
                        user_to_be_update.save()
                        LOG_INFO('User  %s   Updated ',user_to_be_update.user_name)
                        return WebReturn(self,httplib.OK,get_list_obj_to_update,{'User':'Password changed successfully'})        
                    else:
                        LOG_ERROR('Password not matched   %s ',user_name )
                        return WebReturn(self,httplib.FORBIDDEN,get_list_obj_to_update,{'User':'Password not matched'})  
                else:
                    LOG_WARNING('Validation error  with  %s  ',get_list_obj_to_update )
                    
                    return WebReturn(self,httplib.BAD_REQUEST,get_list_obj_to_update,{'User':user_name})    
                
                    
class OrgUsersHandler(tornado.web.RequestHandler):   
    "getting list of all user belongs to a particular organization by name "
    @ValidatedAPI  
    def get(self, org_name):
        try:
            org_exist = Organization.objects.get(org_name=org_name)  
        except:
            LOG_WARNING('Organization %s is not present in our database', org_name )
            
            org_exist = False
            return  WebReturn(self,httplib.FORBIDDEN,{'reason':'organization does not exist'},{'organization':org_name})   
        if org_exist.status == 0:
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'This user does not longer  exist'},{'Username':org_name}) 
        if org_exist:
            get_users = User.objects.defer("passwd","status").filter(org_name=org_exist).exclude(status=0)            
            if not len(get_users) == '0':
                get_dump = jsonpickle.encode(get_users)
                self.write(get_dump)
                LOG_INFO('Organization details ', get_dump )
                     
            else:
                return  WebReturn(self,httplib.OK,{'reason':'Sorry no user added to this organization till yet '},{'organization':org_name})   
            


class MeetingHandler(tornado.web.RequestHandler):
    @ValidatedAPIFORMEETING  
    def post(self):
        
        data_to_verify = ['timezone','meeting_datetime','sms_no','name','meetingID','status','recurring_time','duration','date_expired','attendee_passwd', 'moderator_passwd', 'created_by', 'sms_no', 'meeting_logout_url', 'max_participants'] 
        meeting_id = 'REG_'+str(random.randint(56,1000222222))
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s ',meeting_id )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':meeting_id})
            get = None  
        get = dict2obj(body_loads)
      
        if get :
            try:check_exist = Meeting.objects.get(meetingID = meeting_id) 
            except:check_exist = False
            if check_exist:
                LOG_WARNING('Meeting %s already exist ',meeting_id )
                return  WebReturn(self, httplib.CONFLICT,{'reason':'Meeting is already exist please chose another meeting name '},{'MeetingID':meeting_id})   

            if not check_exist:
                mtng = Meeting()
                mtng.meetingID = meeting_id
                for k,v in body_loads.items():
                    if k in data_to_verify:
                        try:
                            setattr(mtng, k, v)
                        except:
                            pass
            get_validate = mtng.Validate()
            if get_validate == {}:
                try:check_user_exist = User.objects.get(user_name=get.created_by)
                except:
                    check_user_exist = False
                    return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'User does not exist '},{'MeetingID':meeting_id})   
                if check_user_exist:
                    mtng.created_by = check_user_exist
                    try:
                        mtng.conferenceID = random.randint(2,8)
                        getbbb_class = BBBMeeting(mtng.meetingID,mtng.name,mtng.moderator_passwd,mtng.attendee_passwd,GLOBAL_URL_LOGOUT + str(mtng.meetingID))
                        get_start_info  = getbbb_class.start()
                        if getbbb_class.messageKey == {}:
                            mesg = 'Meeting created successfully'
                            y = None
                        else:
                            y = getbbb_class.messageKey  
                            mesg =  getbbb_class.messageKey   
                        mtng.save()  
                        MeetingRecord(check_user_exist,mtng,'NULL',mesg,mtng.meetingID)
                        return WebReturn(self,httplib.OK, y,get_start_info)   
                    except:
                        import sys
                        print sys.exc_info()
                        
                        return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
                #return  WebReturn(self, httplib.OK,y ,{'reason':'Meeting created succesfully'})   
            
            else:
                LOG_WARNING('Validation error  with  %s  ',get_validate )
                return  WebReturn(self,httplib.FORBIDDEN, get_validate,{'Meeting':meeting_id})   
                           
            
    @ValidatedAPI      
    def delete(self, meeting_id):
        "This method is used to end  a meeting "
        data_to_verify = ['moderator_passwd'] 
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Please enter correct body format ')
            return WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':"Body is not define "},{'data':meeting_id})
            get = None  
        get = dict2obj(body_loads)
        if get:
            try:
                Chk_org =  Meeting.objects.get(meetingID=meeting_id,moderator_passwd = get.moderator_passwd)
            except:
                Chk_org = False
                return WebReturn(self,httplib.BAD_REQUEST,{'reason':'Meeting  does not exist'},{'Meeting':meeting_id})   
            if Chk_org.status == 0:
                return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'This meeting does not longer  exist'},{'meetingID':meeting_id}) 
            
            if Chk_org:
                getbbb_class = BBBMeeting(Chk_org.meetingID,Chk_org.name,Chk_org.moderator_passwd,Chk_org.attendee_passwd,GLOBAL_URL_LOGOUT + str(Chk_org.meetingID))
                get_result =  getbbb_class.end_meeting()  
                if get_result == 'error':
                    WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'Could not delete the meeting '},{'Meeting':meeting_id})   
                else:
                    mesg = {'Meeting':'Selected meeting ended successfully'}
                    y = None
                    MeetingRecord('NULL',Chk_org,'NULL',mesg,meeting_id)
                    Chk_org.status = 0
                    Chk_org.disable_from_meeting()
                    Chk_org.save()
                    return WebReturn(self,httplib.OK,y,mesg)   

    @ValidatedAPI  
    def get(self,meeting_id):
        try: get_meeting_exist = Meeting.objects.get(meetingID=meeting_id)
        except:
            get_meeting_exist = False 
            return WebReturn(self, httplib.BAD_REQUEST,{'reason':'Meeting  does not exist'},{'Meeting':meeting_id})   
        if get_meeting_exist.status == 0:
            return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'This meeting does not longer  exist'},{'meetingID':meeting_id}) 
        if get_meeting_exist:
            try:
                getbbb_class = BBBMeeting(get_meeting_exist.meetingID,get_meeting_exist.name,get_meeting_exist.moderator_passwd,get_meeting_exist.attendee_passwd,GLOBAL_URL_LOGOUT + str(get_meeting_exist.meetingID))
                get_info  = getbbb_class.meeting_info()
                if getbbb_class.messageKey == {}:
                    y = None
                elif getbbb_class.messageKey == "notFound":
                    y = None
                    get_info = getbbb_class.start()
                    mesg =  getbbb_class.messageKey   
                #else:
                    # y = getbbb_class.messageKey  
                    #  mesg =  getbbb_class.messageKey 
                return  WebReturn(self,httplib.OK,y,get_info)   

            except:
                LOG_WARNING('We are Unable to process your request ' )
                return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request '},{'Meeting':meeting_id})   
    @ValidatedAPI  
    def put(self,meeting_id):
        data_to_verify = ['timezone','meeting_datetime','sms_no','name','meetingID','status','recurring_time','duration','date_expired','attendee_passwd', 'moderator_passwd', 'created_by', 'sms_no', 'meeting_logout_url', 'max_participants'] 
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':meeting_id})
            get = None  
        print body_loads    
        get = dict2obj(body_loads)
        if get :
            try:check_exist = Meeting.objects.get(meetingID = meeting_id) 
            except:return  WebReturn(self, httplib.BAD_REQUEST,{'reason':'Meeting does not exist '},{'Meeting':meeting_id})
            if check_exist.status == 0:
                return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'This meeting does not longer  exist'},{'meetingID':meeting_id}) 
            if check_exist:
                    for k,v in body_loads.items():
                        if k in data_to_verify:
                            try:
                                setattr(check_exist, k, v)
                            except:pass
                    get_validate = check_exist.Validate()
                    if get_validate == {}:
                        try:check_user_exist = User.objects.get(user_name=get.created_by)
                        except:
                            check_user_exist = False
                            return  WebReturn(self, httplib.BAD_REQUEST, {'reason':'User does not exist '},{'Meeting':meeting_id})   
                        if check_user_exist:
                            try:
                                getp = Participant.objects.filter(meeting_id = check_exist.id)
                                getp.delete()
                                check_exist.conferenceID = random.randint(2,5000)
                                getbbb_class = BBBMeeting(check_exist.meetingID,check_exist.name,check_exist.moderator_passwd,check_exist.attendee_passwd,GLOBAL_URL_LOGOUT + str(check_exist.meetingID))
                                get_start_info  = getbbb_class.start()
                                if getbbb_class.messageKey == {}:
                                    mesg = 'Meeting Updated successfully'
                                    y = None
                                else:
                                    y = getbbb_class.messageKey  
                                    mesg =  getbbb_class.messageKey   
                                check_exist.save()
                                MeetingRecord(check_user_exist,check_exist,'NULL',mesg,check_exist.meetingID)
                                return  WebReturn(self,httplib.OK,y,get_start_info)   
                
                            except:
                                return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
                    else:
                        LOG_WARNING(get_validate)
                        return  WebReturn(self,httplib.BAD_REQUEST,get_validate,{'Meeting':meeting_id})   
                                       
                        
def MeetingRecord(userid,mtngid,participantid,mesg,miscellaneous):
    obj =  Usage()
    try:obj.user = userid
    except:pass
    try:obj.meeting = mtngid
    except:pass
    try:obj.action = mesg
    except:pass
    try:obj.participant = participantid
    except:pass
    try:obj.miscellaneous = miscellaneous
    except:pass
    obj.save()    
    
class MeetingroomParticipantHandler(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self,meeting_id,name):
        data_to_verify = ['password','email','contact_no']
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s ',meeting_id )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'Please enter the body in json format '},{'Meeting':meeting_id})            
            get = None  
        get = dict2obj(body_loads)
        if get :
            try:meeting_obj = Meeting.objects.get(meetingID =meeting_id)
            except:
                meeting_obj = False
                WebReturn(self,httplib.BAD_REQUEST,{'reason':'Meeting does not exist '},{'Meeting':meeting_id})               
          #  if meeting_obj.status == 0:
           #     return  WebReturn(self,httplib.FORBIDDEN,{'reason':'This meeting does not longer  exist'},{'meetingID':meeting_id}) 
            try:check_exist = Participant.objects.get(name = name,meeting_id = meeting_obj).exclude(status = 0)
            except:check_exist = False
            if  check_exist:
               return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'Participant is already added for this meeting '},{'Meeting':meeting_id})
            else:
                obj = Participant()
                obj.name = name
             
            for k,v in body_loads.items():
                if k in data_to_verify:
                    try:
                        setattr(obj, k, v)
                    except:
                        pass    
            get_validate = obj.Validate() 
            print get_validate
            if get_validate == {}:
                 try:meeting_obj.attendee_passwd == obj.password
                 except: return WebReturn(self,httplib.UNAUTHORIZED,{'reason':'Invalid attendee password '},{'Meeting':meeting_id})   
                 try:
                    getbbb_class = BBBMeeting(meeting_obj.meetingID,meeting_obj.name,meeting_obj.moderator_passwd,meeting_obj.attendee_passwd,'http://talismanuc.com')
                    get_start_info  = getbbb_class.moderator_join_url(obj.name)
                    key = random.randint(2,5000)
                    obj.notification_url =  get_start_info 
                    obj.user_view__url = LOCALMACHINE + '/meeting/' +meeting_obj.meetingID + '/join?key = ' + str(key)  
                    obj.key = key
                    obj.meeting_id= meeting_obj
                    obj.save()
                    MeetingRecord('NULL',meeting_obj,obj,'Participant added successfully',obj.name)
                    y = None
                    return  WebReturn(self,httplib.OK,y,obj.key)   
                 except:
                    return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
            else:
                return  WebReturn(self,httplib.BAD_REQUEST,get_validate,{'Meeting':meeting_id})   
    

class MeetingParticipantHandler(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self,meeting_id,name):
        data_to_verify = ['password','email','contact_no']
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s ',meeting_id )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'Please enter the body in json format '},{'Meeting':meeting_id})            
            get = None  
        get = dict2obj(body_loads)
        print body_loads
        if get :
            try:meeting_obj = Meeting.objects.get(meetingID =meeting_id)
            except:
                meeting_obj = False
                WebReturn(self,httplib.BAD_REQUEST,{'reason':'Meeting does not exist '},{'Meeting':meeting_id})               
          #  if meeting_obj.status == 0:
           #     return  WebReturn(self,httplib.FORBIDDEN,{'reason':'This meeting does not longer  exist'},{'meetingID':meeting_id}) 
            try:check_exist = Participant.objects.get(name = name,meeting_id = meeting_obj).exclude(status = 0)
            except:check_exist = False
            if  check_exist:
               return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'Participant is already added for this meeting '},{'Meeting':meeting_id})
            else:
                obj = Participant()
                obj.name = name
             
            for k,v in body_loads.items():
                if k in data_to_verify:
                    try:
                        setattr(obj, k, v)
                    except:
                        pass    
            get_validate = obj.Validate() 
            print get_validate
            if get_validate == {}:
             	try:meeting_obj.attendee_passwd == obj.password
             	except: return WebReturn(self,httplib.UNAUTHORIZED,{'reason':'Invalid attendee password '},{'Meeting':meeting_id})   
                try:
                    getbbb_class = BBBMeeting(meeting_obj.meetingID,meeting_obj.name,meeting_obj.moderator_passwd,meeting_obj.attendee_passwd,GLOBAL_URL_LOGOUT +str(meeting_obj.meetingID))
                    # generate moderate code 
                    if get.privilege == 'M':
                        get_start_info  = getbbb_class.moderator_join_url(obj.name)
                        key = random.randint(2,5000)
                        obj.notification_url =  get_start_info 
                        obj.user_view__url = LOCALMACHINE + '/meeting/' +meeting_obj.meetingID + '/join?key = ' + str(key)  
                        obj.key = key
                        obj.meeting_id= meeting_obj
			obj.previlage = 'M'
                        obj.save()
                        MeetingRecord('NULL',meeting_obj,obj,'Participant added successfully',obj.name)
                        y = None
                        return  WebReturn(self,httplib.OK,y,obj.key)   
                    #=============================== end
                    else:
                        get_start_info  = getbbb_class.join_url(obj.name)
                        key = random.randint(2,5000)
                        obj.notification_url =  get_start_info 
                        obj.user_view__url = LOCALMACHINE + '/meeting/' +meeting_obj.meetingID + '/join?key = ' + str(key)  
                        obj.key = key
			obj.previlage = 'A'
                        obj.meeting_id= meeting_obj
                        obj.save()
                        MeetingRecord('NULL',meeting_obj,obj,'Participant added successfully',obj.name)
                        y = None
                        return  WebReturn(self,httplib.OK,y,obj.key)   
                except:
                    return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
            else:
                return  WebReturn(self,httplib.BAD_REQUEST,get_validate,{'Meeting':meeting_id})   
    @ValidatedAPI  
    def get(self,meeting_id,name):
        try:get_meeting_exist = Meeting.objects.get(meetingID=meeting_id)
        except:return WebReturn(self, httplib.FORBIDDEN, {'reason':'Meeting  does not exist'},{'Meeting':meeting_id})   
        try: get_participant_exist = Participant.objects.get(name=name)
        except:
            get_participant_exist = False 
            return WebReturn(self, httplib.BAD_REQUEST, {'reason':'Participant  does not exist'},{'Participant':meeting_id})   
        if get_participant_exist.status == 0:
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'This Participant does not longer  exist'},{'meetingID':meeting_id}) 
        if get_participant_exist:
            y = None
            LOG_INFO('Organization details ', get_participant_exist )
            return WebReturn(self,httplib.OK, y,get_participant_exist)   
    @ValidatedAPI      
    def put(self,meeting_id,name):
        data_to_verify = ['notification_pin','email']
        try:
            body = self.request.body
            body_loads = loads(body)            
        except:    return WebReturn(self,httplib.FORBIDDEN, {'reason':"Body is not define "},{'data':meeting_id})
        get = dict2obj(body_loads)
        if get:
            try:get_participant = Participant.objects.get(name = name)
            except:
                LOG_WARNING('Participant  you are looking for does not exist')
                return WebReturn(self,httplib.NOT_FOUND, {'reason':"Participant is not present in our database "},{'name':name})
            if get_participant.status == 0:
                LOG_WARNING('Participant  you are looking doest not longer exist his status is inactive ')
                return  WebReturn(self,httplib.NOT_FOUND,{'reason':'This Participant does not longer  exist'},{'meetingID':meeting_id}) 
            try:get_meeting_id = Meeting.objects.get(meetingID = meeting_id)
            except:return WebReturn(self,httplib.FORBIDDEN, {'reason':"Meeting does not exist"},{'name':name})
            if get_meeting_id.status == 0:
                LOG_WARNING('Meeting %s does not exist',get_meeting_id)
                return  WebReturn(self,{'reason':'This meeting does not longer  exist'},{'meetingID':meeting_id}) 
            for k,v in body_loads.items():
             if k in data_to_verify:
                try:
                    setattr(get_participant, k, v)
                except:
                    pass    
            get_validate = get_participant.Validate() 
            if get_validate == {}:
                try:get_name = get.new_name     
                except:get_name = False
                if get_name == '':
                    return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'Participant name can not be blank '},{'meetingID':meeting_id}) 
                if get_name :
                    try:check_exist = Participant.objects.get(name = get_name)
                    except:
                        check_exist = False
                        pass
                    if check_exist and not check_exist.status == 0 :
                        return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'Participant name can not be override '},{'Meeting':meeting_id})
                    if get_name == '':
                        return WebReturn(self,httplib.BAD_REQUEST,{'reason':'Please enter the participant new name to change'},{'Meeting':meeting_id})
                    else:
                        get_participant.name = get_name
                        try:get_meeting_id.attendee_passwd == get_participant.password
                        except: return WebReturn(self,httplib.UNAUTHORISED,{'reason':'Invalid attendee password '},{'Meeting':meeting_id})   
                        try:
                            getbbb_class = BBBMeeting(get_meeting_id.meetingID,get_meeting_id.name,get_meeting_id.moderator_passwd,get_meeting_id.attendee_passwd,GLOBAL_URL_LOGOUT +str(get_meeting_id.meetingID))
                            get_start_info  = getbbb_class.join_url(get_participant.name)
                            if get_start_info:
                                get_participant.notification_url =  get_start_info 
                                get_participant.meeting_id= get_meeting_id
                                get_participant.save()
                                y = None
                                LOG_INFO('Participant  named  %s  added successfully',get_participant.name)
                                MeetingRecord('NULL',get_meeting_id,get_participant,'Participant updated successfully',get_participant)
                                return  WebReturn(self,httplib.OK,y,{'reason':'Participant Updated succesfully'})   
                        except:
                            LOG_WARNING('Internal server error')
                            return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
            else:
                LOG_INFO('PLease fill all the required field ',get_validate)
                return  WebReturn(self,httplib.NOT_ACCEPTABLE,get_validate,{'Meeting':meeting_id})   
    @ValidatedAPI  
    def delete(self,meeting_id,name):
        try:get_meeting_id = Meeting.objects.get(meetingID = meeting_id)
        except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"Meeting does not exist"},{'name':name})
        if get_meeting_id.status == 0:
            LOG_INFO('Participant  named %s  does not longer  exist',name)
            return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'This participant does not longer  exist'},{'meetingID':meeting_id}) 

        try:get_participant = Participant.objects.get(name = name)
        except:return WebReturn(self,httplib.UNAUTHORIZED,{'reason':"Participant is not present in our database "},{'name':name})
        if name and meeting_id:
  	           get_absoulute = Participant.objects.get(name =get_participant.name  ) 
	           get_absoulute.delete()
	           y = None
          #     LOG_INFO('Participant %s deleted succesfully' % name )
	           return  WebReturn(self,httplib.OK,y,{'Meeting':'Selected participant deleted successfully'})   


class AdminHandler(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self,user_name):
        data_to_verify = ['role','user_type','middle_name','first_name','city','state','country','street','pin','last_name','first_name', 'passwd', 'address', 'date_created', 'primary_mobile', 'primary_landline', 'primary_email','secondary_mobile', 'secondary_landline','secondary_email']
        try:user_name 
        except:
            LOG_WARNING('Invalid Url parameter ', + str(user_name))
            return WebReturn(self,httplib.FORBIDDEN, {'reason':"Please pass a user name along url "},{'data':user_name})
        if user_name == '':
            return WebReturn(self,httplib.FORBIDDEN,{'reason':'Please pass a valid user name along url'},{'data':user_name})
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s',user_name )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':user_name})
            get = None  
        get = dict2obj(body_loads)
        try:get_org = Organization.objects.get(org_name = get.org_name)
        except:
            LOG_WARNING('This organization doest not exist in our database ')
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Please enter a valid organization name "},{'data':user_name})
        if get_org.status == 0:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"This organization doest not longer exist  "},{'data':get.org_name})
        usr = User()
        for k,v in body_loads.items():
            if k in data_to_verify:
                try:
                    setattr(usr, k, v)
                except:
                    pass
        get_validate_response = usr.Validate() 
        if get_validate_response == {}:
            usr.passwd = hashlib.sha1(usr.passwd).hexdigest()
            try:get_already = User.objects.get(user_name =user_name)
            except:get_already = False
            if not get_already:
                usr.user_name = user_name
               
                usr.org_name = get_org
                usr.save()
                LOG_INFO('User named %s  has been added ',usr.user_name)
            else:
                return WebReturn(self,httplib.BAD_REQUEST,{'reason':'User already exist '},{'user_name':'User already registered '})
            y = None 
            MeetingRecord(usr,'NULL','NULL','User created successfully ','user name' + usr.user_name)
            return WebReturn(self,httplib.OK,y,{'Message ':'Data Added successfully'})   
        else:
            LOG_WARNING('Validation error  with  %s  ',get_validate_response )
            return WebReturn(self,httplib.BAD_REQUEST,get_validate_response,{'User':user_name})              
    @ValidatedAPI  
    def get(self, user_name):
        try:
            check_exist = User.objects.defer("passwd").get(user_name=user_name)
        except:
            check_exist = False
        if not  check_exist:
            LOG_WARNING('Username %s doest not exist   ',user_name )
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'User does not exist'},{'Username':user_name})   
        if check_exist.status == 0:
            return  WebReturn(self,httplib.UNAUTHORIZED,{'reason':'This user does not longer  exist'},{'Username':user_name})   
        else :
            y = None
            LOG_INFO('User details :',  check_exist )            
            return  WebReturn(self,httplib.OK,y,check_exist)   
    @ValidatedAPI  
    def delete(self, user_name):
        try:
            Chk_usr =  User.objects.get(user_name=user_name)   
        except:
            Chk_usr = False
            LOG_WARNING('Username %s doest not exist   ',user_name )
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':'user_name does not exist'},{'Username':user_name})   
        if Chk_usr.status == 0:
            return  WebReturn(self,httplib.UNAUTHORIZED,{'reason':'This user does not longer exist'},{'Username':user_name}) 
        if Chk_usr:
            Chk_usr.status = 0
            Chk_usr.disable_from_user()
           
            LOG_INFO('User %s  has been deleted successfully  ' % user_name )            
            MeetingRecord(Chk_usr,'NULL','NULL','User deleted  successfully ','user name \t' + Chk_usr.user_name)
            Chk_usr.save()
            return WebReturn(self,httplib.OK,{'errors':'None'},{'Success':' user has been deleted successfully'})   
        
    @ValidatedAPI  
    def put(self, user_name):
        data_to_verify = ['role','user_type','middle_name','first_name','city','state','country','street','pin','last_name','first_name', 'passwd', 'address', 'date_created', 'primary_mobile', 'primary_landline', 'primary_email','secondary_mobile', 'secondary_landline','secondary_email']

        try:user_name 
        except:
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Please pass a user_name name along url "},{'data':user_name})
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s ',user_name )
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':user_name})
            get = None  
        get = dict2obj(body_loads)
        
        try:get_first_name = get.first_name
        except:get_first_name = False
        try:
            usr = User.objects.get(user_name=user_name)
        except:
            LOG_INFO('User %s is not present in our database', user_name )
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Username does not exist "},{'data':user_name})
        if usr.status == 0:
            return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'This user does not longer  exist'},{'Username':user_name})         
        for k,v in body_loads.items():
            if k in data_to_verify:
                try:
                    setattr(usr, k, v)
                except:
                    pass
        get_validate_response = usr.Validate()  
        if get_validate_response == {}:
            if get_first_name : usr.first_name = get_first_name
            usr.save()
            y = None 
            LOG_INFO('User  %s   Updated ',usr.user_name)
            MeetingRecord(usr,'NULL','NULL','User update   successfully ','user name \t' + usr.user_name)
            return WebReturn(self,httplib.OK,y,{'Message ':'Data Updated successfully'})   
        else:
            LOG_WARNING('Validation error  with  %s  ',get_validate_response )
            
            return WebReturn(self,httplib.BAD_REQUEST,get_validate_response,{'User':user_name})        

class Notification(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self,meeting_id):
        try:verify_exist = Meeting.objects.get(meetingID = meeting_id)
        except:
            verify_exist = False
            return WebReturn(self,{'reason':"Meeting does not exist "},{'data':meeting_id})
        if verify_exist.status == 0:
            return WebReturn(self,{'reason':"Meeting does not longer exist "},{'data':meeting_id})
        sender = EMAIL_HOST_USER #verify_exist.created_by.primary_email
        subject = 'Invitation to Conference'
        get_all_participant = Participant.objects.filter(meeting_id = verify_exist).exclude(status = 0)
        if not get_all_participant == []:
            
            for get_single in get_all_participant:
                message = message_template % ( get_single.name, verify_exist.created_by.user_name,verify_exist.meeting_datetime,
                                                           verify_exist.name,verify_exist.sms_no, verify_exist.conferenceID,
                                                           get_single.user_view__url,
                                                           get_single.password)
                try:
                    MeetingRecord('NULL',verify_exist,get_single,'Email successfully sent',message)
                    send_mail(subject, message,sender, (get_single.email,), fail_silently=False)    
                except:pass
                try:
                    MeetingRecord('NULL',verify_exist,get_single,'Sms successfully sent',sms_text % (verify_exist.created_by.user_name,verify_exist.meeting_datetime,verify_exist.sms_no,get_single.notification_pin))
                    send_sms(get_single.contact_no,sms_text % (verify_exist.created_by.user_name,verify_exist.meeting_datetime,verify_exist.conferenceID,get_single.notification_pin))
                except:pass
            try:
                MeetingRecord(verify_exist.created_by,verify_exist,get_single,'Email successfully sent',message)
                send_mail(subject, message,sender, (verify_exist.created_by.primary_email,), fail_silently=False)
            except:pass
            try:
                MeetingRecord(verify_exist.created_by,verify_exist,'NULL','Sms successfully sent','')
                send_sms(verify_exist.created_by.primary_mobile,sms_text % (verify_exist.created_by.user_name,verify_exist.meeting_datetime,verify_exist.sms_no,verify_exist.sms_no))        
            except:pass
        else:
            import sys 
            LOG_WARNING('Sending notification failed due to %s' % sys.exc_info())
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Error"},{'data':meeting_id})     

class Organizations(tornado.web.RequestHandler):
    @ValidatedAPI  
    def get(self):
        try:
            get_orgs = Organization.objects.all().exclude(status = 0).defer("date_created")
        except:
            get_orgs =  False 
            LOG_WARNING('Please add the organization first ')
            return WebReturn(self,httplib.BAD_REQUEST,{'reason':'organizatin not found please add the organization first '},'None')  
        if get_orgs:
          
            LOG_INFO('list of organization %s ' % get_orgs)
            return WebReturn(self,httplib.OK,None,get_orgs)  

class ActivateOrganization(tornado.web.RequestHandler):
    def put(self):
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format ')
            return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':'None'})
            get = None  
        get = dict2obj(body_loads)
        if get:
            try:org = Organization.objects.get(org_name=get.org_name)
            except:
                org  = False
                LOG_INFO('User %s is not present in our database', get.org_nam )
                return WebReturn(self,httplib.BAD_REQUEST,{'reason':"Organization name  does not exist "},{'data':get.org_nam})
            if org:
                org.is_activated = True
                org.save()
                return WebReturn(self,httplib.OK,None,get.org_name)
        else:
            return WebReturn(self,httplib.INTERNAL_SERVER_ERROR,None,get.org_name)




class MeetingRoomHandler(tornado.web.RequestHandler):
    @ValidatedAPIFORMEETING 
    def post(self):
	print "Atleast this request is comimnnh h"
        data_to_verify = ['timezone','meeting_datetime','sms_no','name','meetingID','status','duration','date_expired','attendee_passwd', 'moderator_passwd', 'created_by', 'sms_no', 'meeting_logout_url', 'max_participants'] 
        meeting_id = str(random.randint(56,1000222222))
        
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Body  is not in correct  format  %s ',meeting_id )
	    print "Here it is coming "	
	    return WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':"Body is not define "},{'data':meeting_id})
      	    
	    get = None  
        get = dict2obj(body_loads)
      	print get
	if get :
            try:check_exist = Meeting.objects.get(meetingID = meeting_id) 
            except:check_exist = False
            if check_exist:
                LOG_WARNING('Meetingroom  %s already exist ',meeting_id )
                return  WebReturn(self, httplib.CONFLICT,{'reason':'Meetingroom is already exist please chose another meeting name '},{'MeetingID':meeting_id})   

            if not check_exist:
                mtng = Meeting()
                mtng.meetingID = get.name+'_'+ meeting_id   # Create a relative meeting name to the meeting room
                for k,v in body_loads.items():
                    if k in data_to_verify:
                        try:
                            setattr(mtng, k, v)
                        except:
                            pass
            get_validate = mtng.Validate()
            
            if get_validate == {}:
                
                try:check_user_exist = User.objects.get(user_name=get.created_by)
                except:
                    check_user_exist = False
                    return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'User does not exist '},{'MeetingID':meeting_id})   
                if check_user_exist:
                    mtng.created_by = check_user_exist
                    """ Put this in a seperate function and validate der letter """
                    try :get.type 
                    except:
                        get.type = False
                        return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'Please define the meeting room type i.e open room or close room'},{'MeetingID':meeting_id})   
                    if get.type == '':
                        return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'Please define the meeting room type i.e open room or close room'},{'MeetingID':meeting_id})
                    try :get.expired_on 
                    except:
                        get.expired_on = False
                        return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'Please enter a expiry date of the meeting'},{'MeetingID':meeting_id})   
                    if get.type == '':
                        return  WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':'Please define the meeting room type i.e open room or close room'},{'MeetingID':meeting_id})
                    
                    mr = MeetingRoom()        
                    mr.type = get.type 
                    mr.name =  get.name
                    mr.expired_on =  get.expired_on
                    mr.user = check_user_exist
                    try:
                        mtng.conferenceID = random.randint(2,8)
                        getbbb_class = BBBMeeting(mtng.meetingID,mtng.name,mtng.moderator_passwd,mtng.attendee_passwd,GLOBAL_URL_LOGOUT +str(mtng.meetingID))
                        get_start_info  = getbbb_class.start()
                        if getbbb_class.messageKey == {}:
                            mesg = 'Meetingroom created successfully'
                            y = None
                        else:
                            y = getbbb_class.messageKey  
                            mesg =  getbbb_class.messageKey  
                        mr.save()  
                        mtng.venue = mr   
                        mtng.save()  
                        MeetingRecord(check_user_exist,mtng,'NULL',mesg,mtng.meetingID)
                        print get_start_info
                        return WebReturn(self,httplib.OK, y,get_start_info)   
                    except:
                         import sys
                         print sys.exc_info()
                         return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
                return  WebReturn(self, httplib.OK,y ,{'reason':'Meeting created succesfully'})   
            
            else:
                LOG_WARNING('Validation error  with  %s  ',get_validate )
                return  WebReturn(self,httplib.FORBIDDEN, get_validate,{'Meeting':meeting_id})   
            
    @ValidatedAPI      
    def delete(self, meeting_id):
        "This method is used to end  a meeting "
        data_to_verify = ['moderator_passwd'] 
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            LOG_WARNING('Please enter correct body format ')
            return WebReturn(self,httplib.NOT_ACCEPTABLE, {'reason':"Body is not define "},{'data':meeting_id})
            get = None  
        get = dict2obj(body_loads)
        if get:
            try:
                Chk_org =  MeetingRoom.objects.get(meetingID=meeting_id,moderator_passwd = get.moderator_passwd)
            except:
                Chk_org = False
                return WebReturn(self,httplib.BAD_REQUEST,{'reason':'MeetingRoom  does not exist'},{'MeetingRoom':meeting_id})   
            if Chk_org.status == 0:
                return  WebReturn(self,httplib.BAD_REQUEST,{'reason':'This MeetingRoom does not longer  exist'},{'meetingID':meeting_id}) 
            
            if Chk_org:
                getbbb_class = BBBMeeting(Chk_org.meetingID,Chk_org.name,Chk_org.moderator_passwd,Chk_org.attendee_passwd)
                get_result =  getbbb_class.end_meeting()  
                if get_result == 'error':
                    WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'Could not delete the MeetingRoom '},{'Meeting':meeting_id})   
                else:
                    mesg = {'MeetingRoom':'Selected MeetingRoom ended successfully'}
                    y = None
                    MeetingRecord('NULL',Chk_org,'NULL',mesg,meeting_id)
                    Chk_org.status = 0
                    Chk_org.disable_from_meeting()
                    Chk_org.save()
                    return WebReturn(self,httplib.OK,y,mesg)   

    @ValidatedAPI  
    def get(self,meeting_id):
        try: get_meeting_exist = MeetingRoom.objects.get(meetingID=meeting_id)
        except:
            get_meeting_exist = False 
            return WebReturn(self, httplib.BAD_REQUEST,{'reason':'MeetingRoom  does not exist'},{'MeetingRoom':meeting_id})   
        if get_meeting_exist.status == 0:
            return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'This MeetingRoom does not longer  exist'},{'meetingID':meeting_id}) 
        if get_meeting_exist:
            try:
                getbbb_class = BBBMeeting(get_meeting_exist.meetingID,get_meeting_exist.name,get_meeting_exist.moderator_passwd,get_meeting_exist.attendee_passwd)
                get_info  = getbbb_class.meeting_info()
                if getbbb_class.messageKey == {}:
                    y = None
                elif getbbb_class.messageKey == "notFound":
                    y = None
                    get_info = getbbb_class.start()
                    mesg =  getbbb_class.messageKey   
                #else:
                    # y = getbbb_class.messageKey  
                    #  mesg =  getbbb_class.messageKey 
                return  WebReturn(self,httplib.OK,y,get_info)   

            except:
                LOG_WARNING('We are Unable to process your request ' )
                return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request '},{'MeetingRoom':meeting_id})   
    @ValidatedAPI  
    def put(self,meeting_id):
        data_to_verify = ['timezone','meeting_datetime','sms_no','name','meetingID','status','recurring_time','duration','date_expired','attendee_passwd', 'moderator_passwd', 'created_by', 'sms_no', 'meeting_logout_url', 'max_participants'] 
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':meeting_id})
            get = None  
        get = dict2obj(body_loads)
        if get :
            try:check_exist = Meeting.objects.get(meetingID = meeting_id) 
            except:return  WebReturn(self, httplib.BAD_REQUEST,{'reason':'MeetingRoom does not exist '},{'MeetingRoom':meeting_id})
            if check_exist.status == 0:
                return  WebReturn(self,httplib.NOT_ACCEPTABLE,{'reason':'This MeetingRoom does not longer  exist'},{'meetingID':meeting_id}) 
            if check_exist:
                    for k,v in body_loads.items():
                        if k in data_to_verify:
                            try:
                                setattr(check_exist, k, v)
                            except:pass
                    get_validate = check_exist.Validate()
                    if get_validate == {}:
                        try:check_user_exist = User.objects.get(user_name=get.created_by)
                        except:
                            check_user_exist = False
                            return  WebReturn(self, httplib.BAD_REQUEST, {'reason':'User does not exist '},{'MeetingRoom':meeting_id})   
                        if check_user_exist.status == 0:
                            return WebReturn(self,httplib.FORBIDDEN,{'reason':"This user does  not longer exist  "},{'MeetingID':meeting_id})
                        if not check_user_exist.role == 'Admin':
                            return WebReturn(self,httplib.FORBIDDEN,{'reason':"You do not have authority to create a room please contact to your administrator"},{'MeetingID':meeting_id})
                        if check_user_exist:
                            try:
                                check_exist.conferenceID = random.randint(2,5000)
                                getbbb_class = BBBMeeting(check_exist.meetingID,check_exist.name,check_exist.moderator_passwd,check_exist.attendee_passwd)
                                get_start_info  = getbbb_class.start()
                                if getbbb_class.messageKey == {}:
                                    mesg = 'MeetingRoom Updated successfully'
                                    y = None
                                else:
                                    y = getbbb_class.messageKey  
                                    mesg =  getbbb_class.messageKey   
                                check_exist.save()
                                MeetingRecord(check_user_exist,check_exist,'NULL',mesg,check_exist.meetingID)
                                return  WebReturn(self,httplib.OK,y,{'reason':'MeetingRoom Updated succesfully'})   
                
                            except:
                                return  WebReturn(self,httplib.INTERNAL_SERVER_ERROR,{'reason':'We are Unable to process your request now '},{'Meeting':meeting_id})   
                    else:
                        LOG_WARNING(get_validate)
                        return  WebReturn(self,httplib.BAD_REQUEST,get_validate,{'MeetingRoom':meeting_id})   
 
 
class ReturnBBBurl(tornado.web.RequestHandler):
    @ValidatedAPI  
    def post(self):
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':'None'})
            get = None  
        get = dict2obj(body_loads)        
        if get :
            try:check_exist = Participant.objects.get(key = get.key) 
            except:return  WebReturn(self, httplib.BAD_REQUEST,{'reason':'Key does not exist '},{'Participant':'None'})
        if check_exist:
            y = "None"
            get_m = Meeting.objects.get(id =check_exist.meeting_id_id )
            getbbb_class = BBBMeeting(get_m.meetingID,get_m.name,get_m.moderator_passwd,get_m.attendee_passwd,GLOBAL_URL_LOGOUT +str(get_m.meetingID))
            get_start_info  = getbbb_class.meeting_info()
            for r,v in get_start_info.items():
                getbbb_class.meeting_info()
                if v == "FAILED":
                    getbbb_class.start()
            if get_m.start_time == "":        
                start =  getbbb_class.start_time
                
                try:
                    start = int(start)
                    start = start/1000
                    start_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(start)))
                    get_m.start_time = start_time
                    get_m.save()
                except:
                    pass
            return WebReturn(self,httplib.OK,y,check_exist.notification_url)

    """ list of datetime object
    appointments = [(datetime(2012, 5, 22, 10), datetime(2012, 5, 22, 10, 30)),
    
                    (datetime(2012, 5, 22, 12), datetime(2012, 5, 22, 13)),
    
                    (datetime(2012, 5, 22, 15, 30), datetime(2012, 5, 22, 17, 10))]
    
    
    
    hours = (datetime(2012, 5, 22, 9), datetime(2012, 5, 24, 18)) datetime object
    
    
    duration = =timedelta(hours=1)  it should be time delta 
    """
 
def get_slots(hours, appointments, duration):

    slots = sorted([(hours[0], hours[0])] + appointments + [(hours[1], hours[1])])
    li = []
    for start, end in ((slots[i][1], slots[i+1][0]) for i in range(len(slots)-1)):

        assert start <= end, "Cannot attend all appointments"

        while start + duration <= end:
         
            today18 = start.replace(hour=17, minute=59, second=59, microsecond=0)
              
            if start <  today18:
                
               print "{:%H:%M} - {:%H:%M}".format(start, start + duration)
               start += duration
            else:
               start += timedelta(hours = 15)
 
class CheckAvailablity(tornado.web.RequestHandler):
    @ValidatedAPI
    def post(self,name): 
        appointments = [] 
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':'None'})
            get = None  
        get = dict2obj(body_loads)
        try:get_meeting = Meeting.objects.filter(name = name,duration = '60')
        except:
            get_meeting = False
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Meeting is not exist"},{'data':'None'})
        if get_meeting:
            for select_meeting in get_meeting:
                getm = int(select_meeting.duration)
                appointments.append((select_meeting.meeting_datetime,select_meeting.meeting_datetime + timedelta(minutes = getm)))
        getint = int(get.duration)
        duration = timedelta(minutes =getint) 
        hours = (datetime.datetime(2012, 5, 24, 18), datetime.datetime(2012, 5, 22, 9)) # we can take this detail from user 
        get_slots(hours, appointments, duration)

class Logouturl(tornado.web.RequestHandler):
    def get(self,mid):
        try:get_meeting = Meeting.objects.get(meetingID = mid) 
        except:
            get_meeting = False
            self.redirect('http://google.com')
        if get_meeting:
            getbbb_class = BBBMeeting(get_meeting.meetingID,get_meeting.name,get_meeting.moderator_passwd,get_meeting.attendee_passwd,GLOBAL_URL_LOGOUT +str(get_meeting.meetingID))
            get_meeting_info = getbbb_class.meeting_info()
            
            for items,v in get_meeting_info.items():
                if items == 'running':
                    if v == 'false':
                        if getbbb_class.start_time != '0' and getbbb_class.end_time != '0':
                            start =  getbbb_class.start_time
                            end =  getbbb_class.end_time
                            start = int(start)
                            end = int(end)
                            start = float(start/1000)
                            end = float(end/1000)
                            start_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(start)))
                            end_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(end)))
                            duration = datetime.strptime(end_time,'%Y-%m-%d %H:%M:%S') - datetime.strptime(start_time,'%Y-%m-%d %H:%M:%S')
                            get_meeting.start_time = start_time
                            get_meeting.end_time = end_time
                            get_meeting.meeting_duration = duration
                            get_meeting.save()
                            self.redirect(get_meeting.meeting_logout_url)
                            
                    else:
                        pass

class Getreport(tornado.web.RequestHandler):
    @ValidatedAPI
    def post(self):
        getall = []
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':'None'})
            get = None  
        get = dict2obj(body_loads)
        if get:
            try:get_org = Organization.objects.get(org_name = get.org_name)
            except:
                get_org = False
                return WebReturn(self,httplib.FORBIDDEN,{'reason':"The organization that you had passed is not matching to our any record "},{'data':'None'})
            if get_org:
                try:
                    get_valid_user = User.objects.get(user_name = get.user_name)
                    get_valid_user.org_name = get_org
                except:
                    get_valid_user = False  
                    return WebReturn(self,httplib.FORBIDDEN,{'reason':"You are not the user of this organization please enter valid detail"},{'data':'None'})
                if get_valid_user:
                    get_all_user_of_org = User.objects.filter(org_name =get_org)
                    from django.db.models import Q
                    for user_id in get_all_user_of_org:
                        try:get.start
                        except:
                            get.start = False
                            pass
                        try:get.start and get.limit
                        except:
                            get.start = False
                            get.slimit = False
                            pass
                        try:get.limit
                        except:get.limit = False
                        if get.start and get.limit:
                            try:
                                index = int(get.start)
                                limit = int(get.limit)
                                if index == 1:
                                    limit = limit
     				    index = 0
	                        else:                                     
                                    limit = limit + index
                            except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"please enter only integer value from start and limit"},{'data':'None'})
                            get_meetings = Meeting.objects.filter(created_by = user_id.id)[index:limit]
                            for m in get_meetings: print m    
                        elif get.start:
                            try:index = int(get.start)
                            except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"please enter only integer value from start and limit"},{'data':'None'})
                            get_meetings = Meeting.objects.filter(created_by = user_id.id)[index:]
                        elif get.limit:
                            try:index = int(get.limit)
                            except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"please enter only integer value from start and limit"},{'data':'None'})
                            get_meetings = Meeting.objects.filter(created_by = user_id.id)[:index]
                        else:
                            get_meetings = Meeting.objects.filter(created_by = user_id.id)
                        for meeting in get_meetings:
                            if meeting.venue_id ==None:
                                mee_data = {} 
				get_u_name = User.objects.get(id = meeting.created_by_id)
				getdate = meeting.meeting_datetime
                                mee_data['id'] = meeting.id
                                mee_data['date'] = getdate.strftime("%Y-%m-%d")
                                mee_data['end_time'] = meeting.end_time
                                mee_data['meeting_duration'] = meeting.meeting_duration 
                                mee_data['start_time'] = meeting.start_time 
                                mee_data['meetingID'] = meeting.meetingID

                                mee_data['meeting_datetime']  = meeting.meeting_datetime

                                mee_data['name'] = meeting.name
		       
                                gt_usr = User.objects.get(id=meeting.created_by_id)
                                mee_data['created_by'] = gt_usr.user_name


#         mee_data['created_by'] = get_u_name.user_name

                                get_participant  = Participant.objects.filter(meeting_id  = meeting.id)
                                participant_per_meeting =[]
                                for onp in get_participant:
                                    participant_per_meeting.append(onp.name)
                                mee_data['participants'] = participant_per_meeting


                                get_participant_pv  = Participant.objects.filter(meeting_id  = meeting.id)
                                for gp in get_participant_pv:
                                    if gp.name == get_valid_user.user_name:
                                             mee_data['previlage'] = gp.previlage

				




                                getall.append(mee_data) 
                return WebReturn(self,httplib.OK,'None',getall)           




class GetUserreport(tornado.web.RequestHandler):
    @ValidatedAPI
    def post(self):
	import datetime
        from datetime import date
	today_min = datetime.datetime.now()

        getall = []
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':'None'})
            get = None  
        get = dict2obj(body_loads)
        if get:
                try:
                    get_valid_user = User.objects.get(user_name = get.user)
                    user_id  = get_valid_user
                except:
                    get_valid_user = False  
                    return WebReturn(self,httplib.FORBIDDEN,{'reason':"You are not the user of this organization please enter valid detail"},{'data':'None'})
                if get_valid_user:
                        try:get.start
                        except:
                            get.start = False
                            pass
                        try:get.start and get.limit
                        except:
                            get.start = False
                            get.slimit = False
                            pass
                        try:get.limit
                        except:get.limit = False
                        if get.start and get.limit:
                            try:
                                index = int(get.start)
                                limit = int(get.limit)
                                if index == 1:
                                    limit = limit
                                    index = 0
                                else:                                     
                                    limit = limit + index
                            except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"please enter only integer value from start and limit"},{'data':'None'})
                            if get.archived == "1":
					               get_participant_first = Participant.objects.filter(name=user_id.user_name).values_list('meeting_id_id', flat=True)
					               get_meetings = Meeting.objects.filter(id__in = get_participant_first,meeting_datetime__lt = datetime.datetime.now())[index:limit]
                            else:
                                   get_participant_first = Participant.objects.filter(name=user_id.user_name).values_list('meeting_id_id', flat=True)
				   get_meetings = Meeting.objects.filter(id__in = get_participant_first,meeting_datetime__gt = datetime.datetime.now())[index:limit]
                        elif get.start:
                            try:index = int(get.start)
                            except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"please enter only integer value from start and limit"},{'data':'None'})
                            if get.archived =="1": 
                                   get_participant_first = Participant.objects.filter(name=user_id.user_name).values_list('meeting_id_id', flat=True)
                                   get_meetings = Meeting.objects.filter(id__in = get_participant_first,meeting_datetime__lt = datetime.datetime.now())[index:]                                
                            else:
                                   get_participant_first = Participant.objects.filter(name=user_id.user_name).values_list('meeting_id_id', flat=True)
                                   get_meetings = Meeting.objects.filter(id__in = get_participant_first,meeting_datetime__gt = datetime.datetime.now())[index:]                                
                        elif get.limit:
                            try:index = int(get.limit)
                            except:return WebReturn(self,httplib.FORBIDDEN,{'reason':"please enter only integer value from start and limit"},{'data':'None'})
                            if get.archived == "1":
                                   get_participant_first = Participant.objects.filter(name=user_id.user_name).values_list('meeting_id_id', flat=True)
                                   get_meetings = Meeting.objects.filter(id__in = get_participant_first,meeting_datetime__lt = datetime.datetime.now())[:index]                                
                            else:
                                   get_participant_first = Participant.objects.filter(name=user_id.user_name).values_list('meeting_id_id', flat=True)
                                   get_meetings = Meeting.objects.filter(id__in = get_participant_first,meeting_datetime__gt = datetime.datetime.now())[:index]                                
                        else:
			    if get.archived == "1":
				get_meetings =  Meeting.objects.filter(created_by = user_id.id,meeting_datetime__lt =  datetime.datetime.now())
			    else:
				 get_meetings = Meeting.objects.filter(created_by = user_id.id,meeting_datetime__gt = datetime.datetime.now())
                        for meeting in get_meetings:
                            if meeting.venue_id ==None:
                                mee_data = {} 
                                getdate = meeting.meeting_datetime
                                mee_data['id'] = meeting.id
                                mee_data['date'] = getdate.strftime("%Y-%m-%d")
                                mee_data['end_time'] = meeting.end_time
                                mee_data['meeting_duration'] = meeting.meeting_duration 
                                mee_data['start_time'] = meeting.start_time 
                                mee_data['meetingID'] = meeting.meetingID
                                mee_data['start_time'] = meeting.start_time 
                                mee_data['name'] = meeting.name
                                mee_data['meeting_datetime']  = meeting.meeting_datetime
				gt_usr = User.objects.get(id=meeting.created_by_id)
                                mee_data['created_by'] = gt_usr.user_name
                                get_participant  = Participant.objects.filter(meeting_id  = meeting.id)
				participant_per_meeting =[]
				for onp in get_participant:
				    participant_per_meeting.append(onp.name)
				mee_data['participants'] = participant_per_meeting
				get_participant_pv  = Participant.objects.filter(meeting_id  = meeting.id)
				for gp in get_participant_pv:
				    if gp.name == get_valid_user.user_name:
					     mee_data['previlage'] = gp.previlage
				getall.append(mee_data) 
                return WebReturn(self,httplib.OK,'None',getall)       





class GetParticipant(tornado.web.RequestHandler):
    @ValidatedAPI
    def post(self):
        getall = []
        try:
            body = self.request.body
            body_loads = loads(body)
        except:
            return WebReturn(self,httplib.FORBIDDEN,{'reason':"Body is not define "},{'data':'None'})
            get = None  
        get = dict2obj(body_loads)        
        if get:
            try:get_m = Meeting.objects.get(meetingID = get.mid)
            except:
                get_m = False
                return WebReturn(self,httplib.FORBIDDEN,{'reason':"The meeting that you have passed is not matching to our any record "},{'data':'None'})
        if get_m:
            get_participant = Participant.objects.filter(meeting_id = get_m).values('name')
            if get_participant:
                for one in get_participant:
                    getall.append(one)
                return WebReturn(self,httplib.OK,'None',getall)
                
      

        
if __name__ == '__main__':
  tornado.options.parse_command_line()
  app = tornado.web.Application(
  handlers=[
         #  (r'/meeting/(.*)/participants', GetParticipants),       # GET
           
           (r'/meeting/(.*)/notify',Notification),
            (r'/meetingroom/(.*)/participant/(.*)', MeetingroomParticipantHandler), 
           (r'/meeting/(.*)/participant/(.*)', MeetingParticipantHandler),       # POST, GET, PUT, DELETE
           (r'/meetingroom/(.*)',MeetingRoomHandler),
           (r'/meeting/(.*)', MeetingHandler),             # POST, GET, PUT, DELETE
           (r'/checkavailable/(.*)', CheckAvailablity),  # POST
           (r'/user/(.*)/changepassword', UserChangePassword),
           
           (r'/admin/(.*)',AdminHandler),
           (r'/user/(.*)', UserHandler),
           (r'/logout/(.*)', Logouturl),
           (r'/organization/(.*)/users', OrgUsersHandler),      # GET  
         # (r'/organization/activate/', ActivateOrganization),
           (r'/GetReport/',Getreport),
           (r'/GetUserReport/',GetUserreport),
           (r'/GetParticipant/',GetParticipant),
           (r'/returnBBBurl/',ReturnBBBurl),
           (r'/organization/(.*)', OrgHandler),
            
           (r'/organizations/',Organizations) 
         
            
            ],
                                

                              
  template_path=os.path.join(os.path.dirname(__file__), "templates"),
  static_path=os.path.join(os.path.dirname(__file__), "static"),
  debug=True
  )
  http_server = tornado.httpserver.HTTPServer(app)
  http_server.listen(options.port)
  tornado.ioloop.IOLoop.instance().start()



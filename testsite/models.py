import os
os.environ['DJANGO_SETTINGS_MODULE']= "settings"
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime 
from urllib2 import urlopen
from urllib import urlencode
import settings
from django.core.urlresolvers import reverse
import xml.etree.ElementTree as ET
from hashlib import sha1
import random
from meetinghandler import meetinghandler

from utils import *
# Create your models here.

def parse(response):
    try:
        xml = ET.XML(response)
        code = xml.find('returncode').text
        if code == 'SUCCESS':
            return xml
        else:
            raise
    except:
        return None





class Organization(models.Model):
    
    org_name = models.CharField(max_length=30)
    org_super_user = models.ForeignKey('User',related_name = 'user_nm',null = True,blank = True)
    org_type = models.CharField(max_length=30)
    status =models.IntegerField(null=True, blank=True)
    abn_acn_no = models.CharField(max_length = 100)
    org_url = models.CharField(max_length = 120)     
    notes = models.TextField(null=True)
    
    city = models.CharField(max_length=50,null = True,blank = True)
    state = models.CharField(max_length=50,null = True,blank = True)
    country = models.CharField(max_length=50,null = True,blank = True)
    street = models.TextField(null=True,blank = True)
    pin = models.CharField(max_length=30,null = True,blank = True)

 
    date_created = models.DateTimeField(default=datetime.datetime.now,blank=True, null=True)

    is_activated = models.BooleanField(default = False)
    primary_mobile =models.CharField(max_length = 100)
    secondary_mobile =models.CharField(max_length = 100)
    primary_landline =models.CharField(max_length = 100)
    secondary_landline =models.CharField(max_length = 100)
    primary_email = models.CharField(max_length = 100)
    secondary_email = models.CharField(max_length = 100)


    def disable_other(self):
            org_usr = User.objects.filter(org_name=self.id)
            for getone in org_usr:
                getone.status = 0
                getone.save()
                if getone:
                    try:
                        get_mtng = Meeting.objects.filter(created_by = getone)
                        for get_p in get_mtng:
                            get_p.status = 0
                            get_p.save()
                            if get_p:
                                try:
                                    get_participant = Participant.objects.filter(meeting_id =get_p)
                                    for get_pp in get_participant:
                                        get_pp.status = 0
                                        get_pp.save()
                                except:
                                  #  LOG_INFO('Organization %s has no PARTICIPANT IN user ', get_p)
                                    pass        
                    except:
                        pass 


    
    def get_users(self):
        return User.objects.all(org_name = self.org_name)   
       
    class Meta:
        app_label = 'testsite'
       
    
    def Validate(self):
        errors = {}
       # errors.update(check_abn('abn_acn_no',self.abn_acn_no,True)),
        errors.update(get_url('org_url', self.org_url, True))
        errors.update(verifyStringEmpty('city', self.city, True))
        errors.update(verifyStringEmpty('state', self.state, True))
        errors.update(verifyStringEmpty('country', self.country, True))
        errors.update(verifyStringEmpty('street', self.street, True))
        errors.update(verifyStringEmpty('pin', self.pin, True))
        errors.update(verifyStringEmpty('primary_mobile', self.primary_mobile, True))
        errors.update(verifyStringEmpty('primary_landline', self.primary_landline, True))
        errors.update(get_email('primary_email', self.primary_email, True))
        errors.update(verifyStringEmpty('status', self.status, False))
        errors.update(verifyStringEmpty('notes', self.notes, False))
        errors.update(verifyStringEmpty('secondary_mobile', self.secondary_mobile, False))
        errors.update(verifyStringEmpty('secondary_landline', self.secondary_landline, False))
        errors.update(get_email('secondary_email', self.secondary_email, False))

        return errors
   



class User(models.Model):

    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
 #   nick_name = models.CharField(max_length=100)
  #  dob = models.CharField(max_length=100)   
    user_name = models.CharField(max_length=100)
    passwd = models.CharField(max_length=80)
    role = models.CharField(max_length = 20)
    org_name = models.ForeignKey('Organization',related_name = 'user_org_nm',null= False)
    timezone = models.CharField(max_length = 50)
    
    city = models.CharField(max_length=100,null = True,blank = True)
    state = models.CharField(max_length=100,null = True,blank = True)
    country = models.CharField(max_length=100,null = True,blank = True)
    street = models.TextField(null=True,blank = True)
    pin = models.CharField(max_length=30,null = True,blank = True)

    
    user_type = models.CharField(max_length = 100)
    status =models.IntegerField(null=True, blank=True)
    primary_mobile =models.CharField(max_length = 100,null = True,blank = True)
    secondary_mobile =models.CharField(max_length = 100,null = True,blank = True)
    primary_landline =models.CharField(max_length = 100,null = True,blank = True)
    secondary_landline =models.CharField(max_length = 20,null = True,blank = True)
    primary_email = models.CharField(max_length = 100,null = True,blank = True)
    secondary_email = models.CharField(max_length = 100,null = True,blank = True)
    notes = models.TextField()
    date_created = models.DateTimeField(default=datetime.datetime.now,blank=True, null=True)

    date_modified =models.DateTimeField(auto_now = False,null = True)


    def disable_from_user(self):
         try:
            get_mtng = Meeting.objects.filter(created_by = self.id)
            for get_p in get_mtng:
                get_p.status = 0
                get_p.save()
                if get_p:
                    try:
                        get_participant = Participant.objects.filter(meeting_id =get_p)
                        for get_pp in get_participant:
                            get_pp.status = 0
                            get_pp.save()
                    except:
                        LOG_INFO('Organization %s has no PARTICIPANT IN %Ssuser ', get_p)
                        pass        
         except:
            pass 
    
    class Meta:
        app_label = 'testsite'
       
    def Validate(self):
        errors = {}
        errors.update(verifyStringEmpty('first_name', self.first_name, True))        
        errors.update(verifyStringEmpty('last_name', self.last_name, True))
        errors.update(verifyStringEmpty('passwd', self.passwd, True))
        errors.update(verifyStringEmpty('city', self.city, True))
        errors.update(verifyStringEmpty('state', self.state, True))
        errors.update(verifyStringEmpty('country', self.country, True))
        errors.update(verifyStringEmpty('street', self.street, True))
        errors.update(verifyStringEmpty('pin', self.pin, True))
        
        errors.update(validate_timezone('timezone', self.timezone, True))        
        errors.update(verifyStringEmpty('primary_mobile', self.primary_mobile, True))
        errors.update(verifyStringEmpty('primary_landline', self.primary_landline, True))
        errors.update(get_email('primary_email', self.primary_email, True))
        errors.update(verifyStringEmpty('status', self.status, False))
        errors.update(verifyStringEmpty('middle_name', self.middle_name, False))
        errors.update(verifyStringEmpty('notes', self.notes, False))
        errors.update(verifyStringEmpty('secondary_mobile', self.secondary_mobile, False))
        errors.update(verifyStringEmpty('secondary_landline', self.secondary_landline, False))
        errors.update(get_email('secondary_email', self.secondary_email, False))

        return errors
   
    def ValidateAdmin(self):
        errors = {}
        
        errors.update(verifyStringEmpty('last_name', self.last_name, True))
        errors.update(verifyStringEmpty('passwd', self.passwd, True))
        #print self.get_org(self.org_name)
        errors.update(verifyStringEmpty('role', self.role, True))
        errors.update(verifyStringEmpty('address', self.address, True))
        errors.update(verifyStringEmpty('primary_mobile', self.primary_mobile, True))
        errors.update(verifyStringEmpty('primary_landline', self.primary_landline, True))
        errors.update(get_email('primary_email', self.primary_email, True))
        errors.update(verifyStringEmpty('status', self.status, False))
      #  errors.update(utils.verifyStringEmpty('org_type', self.org_type, False))
        errors.update(verifyStringEmpty('middle_name', self.middle_name, False))
        errors.update(verifyStringEmpty('notes', self.notes, False))
        errors.update(verifyStringEmpty('secondary_mobile', self.secondary_mobile, False))
        errors.update(verifyStringEmpty('secondary_landline', self.secondary_landline, False))
        errors.update(get_email('secondary_email', self.secondary_email, False))

        return errors
   

        
class MeetingRoom(models.Model):
    
    name = models.CharField(max_length=100)
    type = models.CharField(max_length = 200)
    expired_on = models.CharField(max_length = 100,blank = True)
    user = models.ForeignKey('User',related_name ='meeting_created',null = True,blank = True)

    class Meta:
        app_label = 'testsite'
                

class Meeting(models.Model):
    name = models.CharField(max_length=200)
    meetingID = models.CharField(max_length = 50) 
    venue = models.ForeignKey('MeetingRoom',related_name ='meetingroom',null = True,blank = True)   # whether One Time or Recurring
    status =models.IntegerField(null=True, blank=True)
    recurring_time = models.CharField(max_length=50)              # (Recurring date schedule information)
    attendee_passwd = models.CharField(max_length=100)
    moderator_passwd = models.CharField(max_length = 100)
    date_created = models.DateTimeField(auto_now=True)               # it should be auto fill 
   
    meeting_datetime = models.DateTimeField(default=datetime.datetime.now,blank=True, null=True)
    timezone = models.CharField(max_length = 50)
    reminder = models.BooleanField()
    start_time = models.CharField(max_length=100)
    end_time = models.CharField(max_length=100)
    meeting_duration = models.CharField(max_length=100)

    duration = models.CharField(max_length=20)
    created_by = models.ForeignKey('User',related_name ='meetingroom_created')
    sms_no = models.IntegerField()  #participant will call on this number   Unique for all users and participant 
    conferenceID = models.IntegerField(blank=True, null=True)
    meeting_logout_url = models.CharField(max_length=100)
    max_participants = models.IntegerField(blank=True, null=True)
    participants = models.ForeignKey('Participant',related_name = 'parts_of_meetingroom',null = True)

    def disable_from_meeting(self):
        try:
            get_participant = Participant.objects.filter(meeting_id =self.id)
            for get_pp in get_participant:
                get_pp.status = 0
                get_pp.save()
        except:
            LOG_INFO('Organization %s has no PARTICIPANT IN %Ssuser ', self.id)
            pass  
    class Meta:
        app_label = 'testsite'
       


    
    def Validate(self):
        errors = {}
        errors.update(get_url('meeting_logout_url', self.meeting_logout_url, True))
        errors.update(verifyStringEmpty('meetingID', self.meetingID, True))
        errors.update(verifyStringEmpty('attendee_passwd', self.attendee_passwd, True))
        errors.update(verify_date('meeting_datetime', self.meeting_datetime))
        errors.update(verifyStringEmpty('moderator_passwd', self.moderator_passwd, True))
                
        errors.update(verifyInteger('sms_no', self.sms_no, True))
        errors.update(verifyStringEmpty('status', self.status, False))
        errors.update(verifyStringEmpty('duration', self.duration, True))
     #   errors.update(verifyStringEmpty('meeting_type', self.meeting_type, False))
        errors.update(verifyInteger('max_participants', self.max_participants, True)) #  verifyInteger  use this method already define if u r able to find the sol 

        return errors




class MeetingNotificationHistory(models.Model):
    meeting_id = models.ForeignKey('Meeting',related_name = 'meeting_notification')
    email     = models.CharField(max_length=30)
    sms_sent = models.CharField(max_length=150)
    sent_to   = models.ForeignKey('Participant',related_name = 'receiver_name')
    sent_on   = models.DateTimeField(auto_now=True)
    sent_by   = models.ForeignKey('User',related_name = 'which_usr_sent')

    class Meta:
        app_label = 'testsite'
       

class Participant(models.Model):
    
    name = models.CharField(max_length=100)
    meeting_id = models.ForeignKey('Meeting',related_name = 'attendee')
    password = models.CharField(max_length=100)
    notification_url = models.CharField(max_length=250)  ###
    user_view__url = models.CharField(max_length = 300)  #### This is what user will get 
    key = models.CharField(max_length = 200)
    contact_no = models.CharField(max_length = 50)
    #notification_pin = models.IntegerField(max_length=100)
    type = models.CharField(max_length=100)
    status =models.IntegerField(null=True, blank=True)
    email = models.EmailField(max_length = 200)
    previlage =  models.CharField(max_length=30)
    class Meta:
        app_label = 'testsite'
  
    
    def Validate(self):
        errors = {}
        
        errors.update(verifyStringEmpty('contact_no', self.contact_no, True))
        errors.update(verifyStringEmpty('password', self.password, True))
       # errors.update(verifyInteger('notification_pin', self.notification_pin, True))
        errors.update(get_email('email', self.email, True))

        return errors

       
    
class AddressBook(models.Model):
    """"  this table will use to store the contactS of a user address book""" 
    name = models.ForeignKey('User',related_name = 'addr_book_of_a_usr')
    date_added = models.DateTimeField(auto_now  = True)
    recipent_name = models.CharField(max_length=20)  
    recipent_url_name = models.CharField(max_length=20)   #   chek it once 
    address = models.TextField()
    pin = models.CharField(max_length = 12)
    primary_mobile =models.CharField(max_length = 12)
    secondary_mobile =models.CharField(max_length = 12)
    primary_landline =models.CharField(max_length = 12)
    secondary_landline =models.CharField(max_length = 12)
    primary_email = models.CharField(max_length = 30)
    secondary_email = models.CharField(max_length = 30)
    notes = models.TextField()
    type = models.CharField(max_length=20)
    status =models.IntegerField(null=True, blank=True)

    class Meta:
        app_label = 'testsite'
       

	


class Usage(models.Model):
    user = models.ForeignKey('User',related_name = 'usage_user',null = True)
    meeting = models.ForeignKey('Meeting',related_name = 'meeting_usages',null = True)
    participant= models.ForeignKey('Participant',related_name = 'meeting_participant_id',null = True)
    date = models.DateTimeField(auto_now  = True)
    action = models.CharField(max_length = 500 ,null = True)
    miscellaneous = models.CharField(max_length = 500)
    
    class Meta:
        app_label = 'testsite'
       


class MeetingLog(models.Model):
    meeting_id = models.ForeignKey('Meeting',related_name = 'meeting_log')
    last_login = models.CharField(max_length=30)
    login_ip =   models.IPAddressField() 
    modified_by = models.ForeignKey('User',related_name = 'modified_bby')
    session = models.CharField(max_length=30) 
    note = models.CharField(max_length=30)
    type = models.CharField(max_length=20)
    status = models.CharField(max_length=20)

    class Meta:
        app_label = 'testsite'
       
    
class Recording(models.Model):

    name = models.ForeignKey('Meeting',related_name = 'meeting_record')
    end_time = models.DateTimeField()
    start_time = models.DateTimeField()
    meeting_url = models.CharField(max_length = 30)
    filepath  = models.CharField(max_length = 60)
    note = models.CharField(max_length = 30)
    type = models.CharField(max_length=20)
    status =models.IntegerField(null=True, blank=True)

    class Meta:
        app_label = 'testsite'
       





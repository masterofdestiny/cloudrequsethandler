# -*- coding: utf-8 -*-
import logging, os

LEVEL_MAP = {
    'debug': logging.DEBUG,
    'error': logging.ERROR,
    'warn': logging.WARNING,
    'info': logging.INFO,
    'critical': logging.CRITICAL
    }

class YCSLogger(object):
    logger = None

    class InnerLogger:
        def __init__(self, loglevel='debug'):
            self.logger = logging.getLogger('YCSLogger')
            self.logger.setLevel(LEVEL_MAP.get(loglevel.lower()))
            logformat = logging.Formatter("%(asctime)s|%(name)s|%(levelname)s|%(module)s|%(funcName)s|#%(lineno)d|%(message)s","%Y-%m-%d %H:%M:%S")
            #ch = logging.StreamHandler()
            #Make sure log directory exists
            outfilepath = './logs/'
            retval = os.path.exists(outfilepath)
            if retval != True:
                os.system('mkdir -p ' + outfilepath)
            logfile = outfilepath + 'log.txt'
            print 'LogFile:', logfile
            ch = logging.FileHandler(logfile)
            ch.setLevel(LEVEL_MAP.get(loglevel.lower()) )
            ch.setFormatter(logformat)
            self.logger.addHandler(ch)
            os.chmod(logfile, 0666)
    
    def __new__(self):
        if not YCSLogger.logger:
            print 'Returning NEW Logger'
            YCSLogger.logger = YCSLogger.InnerLogger().logger
        print 'YCSLogger.logger', YCSLogger.logger    
        return YCSLogger.logger

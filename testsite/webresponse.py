import sys
import os
# sys.path.insert(0, '/home/gpadaki/talisman/billing')
# sys.path.insert(0, os.path.realpath(os.getcwd()))
sys.path.insert(0, os.path.dirname(os.getcwd()))
from tornado.escape import json_decode, json_encode
#import simplejson as json
#from Utils.common import *
from utils import dumps, loads
# from django.core import serializers

from LogMgr.logger import YCSLogger 

logger = YCSLogger()

LOG_DEBUG = YCSLogger.logger.debug
LOG_WARNING = YCSLogger.logger.warning
LOG_FATAL = YCSLogger.logger.fatal
LOG_ERROR = YCSLogger.logger.error
LOG_INFO = YCSLogger.logger.info

import httplib

def jsonresponse(self,object):
    get_json =json_encode(object)
    return get_json
    



class WebReturn(object):
    def __init__(self, webObj,code,params,data,flag=True):
        self.webObj = webObj
        self.code = code
        self.params = params
        self.data = data
        if flag:
            if self.webObj:
                try:
                    self.webObj.set_status(code)
                    self.webObj.set_header("Content-type","application/json")
                    LOG_INFO('Data: ', self.data)
                    js = dumps({'code':self.code,'errors':self.params,'data':self.data})
                    # js = serializers.serialize("json",{'code':self.code,'errors':self.params,'data':self.data})
                    self.webObj.write(js)
                    # print 'Returned :', js
                except Exception as e:
                     print str(e)
   
    def toJSON(self):
        if self.webObj:
            try:
                self.webObj.set_status(code)
                self.webObj.set_header("Content-type","application/json")
                js = dumps({'code':self.code,'errors':self.params,'data':self.data})
                self.webObj.write(js)
             #   print js
                # print 'Returned :', js
            except Exception as e:
                 print str(e)
        else:
            print 'Error....in WebReturn:toJSON()'

    '''
    def toXML(self):
        if self.webObj:
            self.webObj.set_header("Content-type","text/xml")
            js = dumps({'Code':self.code, 'params':self.params})
            self.webObj.write(js)
        else:
            print 'Error....in WebReturn:toJSON()'
    '''

import settings
import urllib
message_template = '''
                Dear %s,
                
                %s has invited you to a tele-conference on the %s.
                
                Agenda: %s
                
                Number to dial: %s
                PIN: %s
                
                Web URL: %s
                password: %s
                
           
                Thank you.
                '''    
sms_text = ''' Tele-conference organised by %s on the %s.
    Dial: %s, PIN code: %s
    Please check your email for further details. '''                
def send_sms(number, text):
    params = {'PhoneNumber' : number, 'PhoneMessage' : text,
              'Username' : settings.SMS_GATEWAY_USER,
              'Pwd' : settings.SMS_GATEWAY_PASSWD }

    url = settings.SMS_API_BASE_URL + urllib.urlencode(params)
    handle = urllib.urlopen(url)
    handle.close()
 
 
                
 #
#      Additional information:
 #                %s
                               

import base64
from hashlib import sha256

def ValidatedAPI(req, **kwargs):
    pass

class APIAuth(object):
    IntSecret = 'VuFlRQv40SUp0y1AXflMD0hWw8ZiiTu08f9ZXc0AYFc='
    ExtSecret = 'xxOy4FgKam9TiD4o+nX7VGT2dziYuLarqGvdXKYROd8='

    # SecretList = [ APIAuth.IntSecret, APIAuth.ExtSecret ]
    SecretList = [ IntSecret, ExtSecret ]


#### Expectation
# All API requests must be accompanied with
# a header called X-Talisman-Secret.
# The values of this header could be IntSecret
# or ExtSecret
#### ------------------------------------


    @staticmethod
    def checkHeader(authHdr):
        # print 'authHdr:', type(authHdr)
        authHeader = {}
        try:
            authHeader = authHdr.__dict__['_as_list']
        except KeyError as k:
            authHeader = {}
        # print 'authHeader:', type(authHeader)
        # print 'AuthHeader Content: ', authHeader
        if type(authHeader) != type({}):
            return False
        secret_msg = ''
        try:
            secret_msg = authHeader['X-Talisman-Secret'][0]
        except KeyError as k:
            #print 'Unable to find Header:'+str(k)
            return False
        except Exception as e:
            # print str(e)
            raise
        else:
            if secret_msg in APIAuth.SecretList:
                return True
            return False
        return False

if __name__ == '__main__':
    a = { 'X-Talisman-Secret' : APIAuth.IntSecret, 'ABC':'Govinda' }
    b = 'GOvinda Padaki'

    print APIAuth.checkHeader(a)
    #print APIAuth.checkHeader(b)

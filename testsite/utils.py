import datetime
import inspect
from lepl.apps.rfc3696 import HttpUrl,Email   # Import to validate the email and url 

from pyxml2obj import XMLin, XMLout
from django.core.exceptions import ValidationError
#from models  import *

from LogMgr.logger import YCSLogger 

logger = YCSLogger()

LOG_DEBUG = YCSLogger.logger.debug
LOG_WARNING = YCSLogger.logger.warning
LOG_FATAL = YCSLogger.logger.fatal
LOG_ERROR = YCSLogger.logger.error
LOG_INFO = YCSLogger.logger.info

import re

try:
    import json
except ImportError:
    import simplejson as json

class JSONDateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        print 'Type...', type(obj)
        
        from django.db.models.query import QuerySet
        from django.core import serializers
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()
        elif isinstance(obj, QuerySet):
            return serializers.serialize("json",obj)
        else:
            try:
                return super(JSONDateTimeEncoder, self).default(obj)
            except:
                return obj.__dict__

def datetime_decoder(d):
    if isinstance(d, list):
        pairs = enumerate(d)
    elif isinstance(d, dict):
        pairs = d.items()
    result = []
    for k,v in pairs:
        if isinstance(v, basestring):
            try:
                v = datetime.datetime.strptime(v, '%Y-%m-%d %H:%M:%S.%f')
            except ValueError:
                try:
                    v = datetime.datetime.strptime(v, '%Y-%m-%dT%H:%M:%S.%f')
                except ValueError:
                    try:
                        v = datetime.datetime.strptime(v, '%Y-%m-%d %H:%M:%S')
                    except ValueError:
                        try:
                            v = datetime.datetime.strptime(v, '%Y-%m-%d').date()
                        except ValueError:
                            pass
        elif isinstance(v, (dict, list)):
            v = datetime_decoder(v)
        result.append((k, v))
    if isinstance(d, list):
        return [x[1] for x in result]
    elif isinstance(d, dict):
        return dict(result)
    
def dumps(obj):   # Python to json
    return json.dumps(obj, skipkeys = True, cls=JSONDateTimeEncoder)

def loads(obj): #Json to python 
    return json.loads(obj, object_hook=datetime_decoder)


def dict2obj(d):
        if isinstance(d, list):
            d = [dict2obj(x) for x in d]
        if not isinstance(d, dict):
            return d
        class C(object):
            pass
        o = C()
        for k in d:
            o.__dict__[k] = dict2obj(d[k])
        return o
    
def verify_data(obj,lst):
    get_error = None
    for x in lst:
        try:
            get_validate =  getattr(obj,x)
        except:
            if get_error is None:
                get_error = []
            get_error.append(x)
    return get_error

def verify_data_old(obj,list):
  #  get_error = None
    for x in list:
        try:
            get_validate =  getattr(obj,x)
        except:
            get_error = []
            get_error= get_error.append(x)
    return get_error


##########################################

def verifyPhone(name, obj, required):
    if name and required:
        return {name : 'Invalid Phone format'}
    else:
        return {}
    
    
##################################   
def verifyInteger(name,obj,required):
    if name and not required :
        print obj
        if not obj == None:
            gett = str(obj).isdigit()
            if gett:
                return {}
            else:
                return {name :'Please enter only digits '}
        if not obj:
            return {} 
        if obj == None:
                return {}          
    if name and required:
        
       if obj == '' :
           return {name :'Please enter only digits '}
       else:
           gett = str(obj).isdigit()
           if gett:
              return {}
           else:
              return {name :'Please enter only digits '}
     
##########################    
def verifyStringEmpty(name, obj,required):
    if name and required:
        if obj == '':
            return {name : 'Should not be blank'}
            
        elif obj == None:
            return {name : 'Should not be blank'}
        else:
            return {}
        
    if name and not required:
        if not obj == '':
            return {}
        if obj == '':
            return {}
        if obj == None:
            return {}
        
            
#################################################### 


def get_url(name,obj,required):
    if name and required:
        validator = HttpUrl()
        try:
            get_true_false = validator(obj) 
        except:
            get_true_false = False
        if get_true_false:
            return {}
        else:
            return {name : 'Please enter a valid url'}
      
def get_email(name,obj,required):
    if name and required:
        email_validator = Email()
        if obj == '':
            return {name : 'Please enter a valid email'}
        else:
            get_true_false =  email_validator(obj)
            if get_true_false:
                return {}
            else:
                return {name : 'Please enter a valid email'}
    if name and not required:
        email_validator = Email()
        if obj == None:
            return {}
        if not obj == '':
                get_true_false =  email_validator(obj)
                if get_true_false:
                    return {}
                else:
                    return {name : 'Please enter a valid email'}
        else :
            return {}    
    """
    elif name and not required:
        email_validator = Email()
        get_true_false =  email_validator(obj)
        if get_true_false:
            return {}
        else:
            {name : 'Please enter a valid email'}
    """
    
def validate_phone(string):
    if not re.match('^\d+$', string) and not re.match('^\+\d+$', string):
        raise ValidationError('Value needs to be all digits or + followed by digits, eg +6145878899')

    
    
 ###############################################   
def verifying_org(obj):    
    x = None
    x = obj.split()
    if not x:
        return None
    else:
        return obj
    
def verifying_date(obj):    
    if not obj:
        return None
    else:
        return obj    
    
def verifying_abn(obj):
    if obj == '':
        return None
    else:
        return obj     

def verify_put_data(obj,list):
 #   print obj.org_url
    get_list = None
    for x in list:
        try:
            get_validate =  getattr(obj,x)
        except:
            get_validate = False
            get_list.append(x)
    return get_list
    
def verify_date(name,obj):
    from datetime import datetime
    try:gt = datetime.strptime(obj, "%Y-%m-%d %H:%M")
    except :
      return {name:'Invalid meeting date!'}
    if gt:
        return {}


def validate_timezone(name,obj,required):
    if name and required:
        if obj =='':
            return {name:"Please enter a valid time zone"}
        elif obj == None:
            return {name:"Please enter a valid time zone"}
        if obj != '':
            gt = obj.startswith('GMT')
            if gt:
                get_lastdigit = obj[-1].isdigit()
                print get_lastdigit
                if get_lastdigit:
                    return {}
                else: return {name:'Please enter the correct GMT time format like GMT + 1'}
            else:return {name:'Please enter the correct utc time format like GMT + 1 or GMT - 4.30'}


def check_abn(name,obj,required):
    if name and required:
        import re
        get_digit = re.sub("\D", "",obj)
        x =  int(str(get_digit)[::-1])
        get_reverse_val = x - 1
        change_it_again = int(str(get_reverse_val)[::-1]) 
        get_first_digit = str(change_it_again)[0]
        print get_digit
        return {name :"Should not be blank"}
   
"""

def convert_keys_to_string(body_loads):
    if not isinstance(body_loads, dict):
        return body_loads
    return dict((str(k), str(v)) 
        for k, v in body_loads.items())
"""
